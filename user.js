const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}




// Q1 Find all users who are interested in video games.

function interestedInVideoGame(obj) {
    let arr = Object.entries(obj);
    let videoGame = arr.filter((curr) => {
        if(String(curr[1].interests) === "Video Games"){
            return true;
        }
    })
    return Object.fromEntries(videoGame);
}
console.log(interestedInVideoGame(users));

// Q2 Find all users staying in Germany.

function stayInGermany(obj) {
    let arr = Object.entries(obj);
    let staying = arr.filter((curr) => {
        if (curr[1].nationality === "Germany"){
            return true;
        }
    })
    return Object.fromEntries(staying);
}

console.log(stayInGermany(users));

// Q3 Sort users based on their seniority level 
//    for Designation - Senior Developer > Developer > Intern




//    for Age - 20 > 10

function sortBasedOnAge (obj){
    let arr = Object.entries(obj);
    let sortedUser = arr.sort((user1, user2) => {
        return user1[1].age > user2[1].age ? -1 : 1;
    });
    return Object.fromEntries(sortedUser);
}
console.log(sortBasedOnAge(users));

// Q4 Find all users with masters Degree.

function userWithMastersDegree(obj) {
    let arr = Object.entries(obj);
    let mastersDegree = arr.filter((curr) => {
        if (curr[1].qualification === "Masters"){
            return true;
        }
    })
    return Object.fromEntries(mastersDegree);
}
console.log(userWithMastersDegree(users));

// Q5 Group users based on their Programming language mentioned in their designation.


